FROM python:3.6.6-slim

RUN apt-get update && apt-get install -y libmcrypt-dev 

COPY requirements.txt .
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

COPY credentials.json .