from django.urls import path, include
from api.views import image_to_text

urlpatterns = [
    path('search_image', image_to_text, name='search_image'),
]