from django.shortcuts import render
from django.conf import settings
from google.cloud import vision
from google.cloud.vision import types
from django.core.files.storage import FileSystemStorage
from django.views.decorators.csrf import csrf_exempt
import os, io
from django.http import JsonResponse
from google.protobuf.json_format import MessageToDict

@csrf_exempt
def image_to_text(request):

    if request.method == 'POST' and request.FILES['product']:
        vision_client = vision.ImageAnnotatorClient()
        myfile = request.FILES["product"]
        fs = FileSystemStorage()
        filename = fs.save(myfile.name, myfile)
        file_name = fs.url(settings.MEDIA_ROOT+'/'+filename)
        with io.open(file_name, "rb") as image_file:
            content = image_file.read()
            image = types.Image(content=content)
        response = vision_client.web_detection(image=image)
        labels = response.web_detection
        response = MessageToDict(labels)
        print(response["bestGuessLabels"][0])
        return JsonResponse(response["bestGuessLabels"][0])        
